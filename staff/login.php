<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();
 
if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <script type="text/JavaScript" src="js/sha512.js"></script> 
    <script type="text/JavaScript" src="js/forms.js"></script>
    <script type="text/javascript" src="../scripts/jquery-1.11.1.min.js"></script>
    <script type="text/JavaScript" src="../bootstrap/dist/js/bootstrap.min.js"></script>

    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<div class="alert alert-warning fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
      <strong>Holy guacamole!</strong> Best check yo self, you're not looking too good.
    </div>
    <div class="container">
        <?php if (isset($_GET['error'])) { ?>
            <div class="alert alert-danger">
                <?php echo $_GET['error'] ?>
            </div>
        <?php } ?>
      <form class="form-signin" role="form" action="includes/process_login.php" method="post" name="login_form">
        <h2 class="form-signin-heading">Please sign in</h2>
        <!--div class="form-group has-feedback">
            <label class="control-label">Username</label>
            <input name="email" type="email" class="form-control" placeholder="Email address" value="kmamagnaye@gmail.com" required autofocus>
            <i class="glyphicon glyphicon-user form-control-feedback"></i>
        </div-->
        
        <input name="email" type="email" class="form-control" placeholder="Email address" value="kmamagnaye@gmail.com" required autofocus>
        <input name="password" id="password" type="password" class="form-control" placeholder="Password" value="Kristel1">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block"
                onclick="formhash(this.form, this.form.password);">Sign in</button>
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
