<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
$data = array();
extract($_GET);
$trans_date = new DateTime($trans_date);

if (isset($update)) {
    
    if(substr($id, 0, 3) == 'new') {
        $insert_query = "INSERT INTO `product` (`product_id`, `description`, `active`, `price`, `inventory`)" .
                        "VALUES ('".$product_id."','".$description."','".$active."', ".$price.",'".$inventory."')";
        
        if (!mysqli_query($mysqli, $insert_query)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli)));
            return;
        }
        
        echo json_encode(array('success' => 'true', 'has_error' => 'false', 'id' => mysqli_insert_id($mysqli)));
        return;
    } else {
        $update_query = "SELECT upper(`active`) as active FROM product WHERE id = " . $id;
        $result = mysqli_query($mysqli, $update_query);
        if (!$result) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli) /*. ' ' . $update_query*/));
            return;
        }
        $row = $result->fetch_assoc();
        
        if ($row['active'] == 'TRUE' && $active == 'FALSE') {
            //TODO: check if there is an active menu item associated.
        }
        
        $update_query = "UPDATE `product` set `description` = '" . $description . "', " .
                                             "`inventory` = '" . $inventory . "', " .
                                             "`active` = '" . $active . "', " .
                                             "`price` = '" . $price . "' " .
                        "WHERE id = '" . $id . "'";
        if (!mysqli_query($mysqli, $update_query)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli) /*. ' ' . $update_query*/));
            return;
        }
        echo json_encode(array('success' => 'true', 'has_error' => 'false', 'id' => $id));
        return;
    }
} else if (isset($_GET['delete'])) {
	// DELETE COMMAND 
	$delete_query = "DELETE FROM `employees` WHERE `EmployeeID`='".$_GET['EmployeeID']."'";	
	$result = mysql_query($delete_query) or die("SQL Error 1: " . mysql_error());
    echo $result;
}

$stmt = $mysqli->prepare("SELECT * FROM sales WHERE trans_date = STR_TO_DATE(?,'%m/%d/%Y')");
if (!$stmt->bind_param('s', $trans_date->format("m/d/Y"))) {
    echo "Bind failed: (" . $stmt->errno . ") " . $stmt->error;
	exit();
}
if (!$stmt->execute()) {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    exit();
}
if (!($res = $stmt->get_result())) {
    echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
    exit();
}

for ($row_no = ($res->num_rows - 1); $row_no >= 0; $row_no--) {
    $res->data_seek($row_no);
	$temp_arr = $res->fetch_assoc();
    $data[] = array('id' => $temp_arr['seq_no'],
					'menu_id' => $temp_arr['item_id'],
					'item_type' => $temp_arr['item_type'],
					'qty' => $temp_arr['qty'],
					'remarks' => $temp_arr['remarks'],
					'total' => $temp_arr['total'],
					'price' => $temp_arr['price']);
}
$stmt->close();

echo "{\"data\":" .json_encode($data). "}";
?>