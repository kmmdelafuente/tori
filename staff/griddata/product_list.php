<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
$data = array();
extract($_GET);

if (!empty($product_id)) {
    $product_id = strtoupper($product_id);
}
if (!empty($active)) {
    $active = strtoupper($active);
} else {
    $active = 'FALSE';
}
if (!empty($inventory)) {
    $inventory = strtoupper($inventory);
} else {
    $inventory = 'FALSE';
}

if (isset($update)) {
    
    if(substr($id, 0, 3) == 'new') {
        $insert_query = "INSERT INTO `product` (`product_id`, `description`, `active`, `price`, `inventory`)" .
                        "VALUES ('".$product_id."','".$description."','".$active."', ".$price.",'".$inventory."')";
        
        if (!mysqli_query($mysqli, $insert_query)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli)));
            return;
        }
        
        echo json_encode(array('success' => 'true', 'has_error' => 'false', 'id' => mysqli_insert_id($mysqli)));
        return;
    } else {
        $update_query = "SELECT upper(`active`) as active FROM product WHERE id = " . $id;
        $result = mysqli_query($mysqli, $update_query);
        if (!$result) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli) /*. ' ' . $update_query*/));
            return;
        }
        $row = $result->fetch_assoc();
        
        if ($row['active'] == 'TRUE' && $active == 'FALSE') {
            //TODO: check if there is an active menu item associated.
        }
        
        $update_query = "UPDATE `product` set `description` = '" . $description . "', " .
                                             "`inventory` = '" . $inventory . "', " .
                                             "`active` = '" . $active . "', " .
                                             "`price` = '" . $price . "' " .
                        "WHERE id = '" . $id . "'";
        if (!mysqli_query($mysqli, $update_query)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli) /*. ' ' . $update_query*/));
            return;
        }
        echo json_encode(array('success' => 'true', 'has_error' => 'false', 'id' => $id));
        return;
    }
}

$stmt = $mysqli->prepare("SELECT * FROM product order by product_id desc");
if (!$stmt->execute()) {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    exit();
}
if (!($res = $stmt->get_result())) {
    echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
    exit();
}

for ($row_no = ($res->num_rows - 1); $row_no >= 0; $row_no--) {
    $res->data_seek($row_no);
    $data[] = $res->fetch_assoc();
}
$stmt->close();

echo "{\"data\":" .json_encode($data). "}";
?>