<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json");
$data = array();

$result = mysqli_query($mysqli,"SELECT * FROM menu order by menu_id");
if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();
}

while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
  $data[] = array('id' => $row['id'],
                  'menu_id' => $row['menu_id'],
                  'description' => $row['description'],
                  'price' => $row['price'],
                  'active' => $row['active']);
}

echo "{\"data\":" .json_encode($data). "}";
?>