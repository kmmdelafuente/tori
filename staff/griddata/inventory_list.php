<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
$data = array();
extract($_GET);

$result = mysqli_query($mysqli,"SELECT * FROM product where active = 'TRUE' and inventory = 'TRUE' order by product_id");
if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();
}

while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
  $data[] = array('id' => $row['id'],
                  'product_id' => $row['product_id'],
                  'description' => $row['description'],
                  'qty_avail' => $row['qty']);
}

echo "{\"data\":" .json_encode($data). "}";
?>