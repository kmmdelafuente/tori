<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
$data = array();
extract($_GET);


$sql = "select p.*" . (!empty($menuId) ? ", m.menu_id, m.qty " : " ") .
       "from product p " . (!empty($menuId) ? "left join menu_items m on p.product_id = m.product_id and m.menu_id = '" . $menuId . "' " : " ") .
       " order by p.product_id";

$result = mysqli_query($mysqli,$sql);
if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    echo $sql;
    exit();
}

while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
  $menuIdVal = 'FALSE';
  $qty = null;
  if(isset($row['menu_id'])) {
    $menuIdVal = $row['menu_id'] == $menuId ? 'TRUE' : 'FALSE';
    $qty = $row['qty'];
  }
  $data[] = array('id' => $row['id'],
                  'product_id' => $row['product_id'],
                  'description' => $row['description'],
                  'active' => $menuIdVal,
                  'qty' => $qty);
}

echo "{\"data\":" .json_encode($data). "} ";
?>