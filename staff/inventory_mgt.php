<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();

if (login_check($mysqli) == false) {
  header('Location: login.php?error=login');
}

$username = get_user($mysqli);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Tori Grill - Product Maintenance</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="../scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../scripts/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="../jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="../jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="../jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.pager.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.sort.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.edit.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.selection.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.columnsresize.js"></script> 
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('.alert .close').on('click', function(e) {
                $(this).parent().hide();
            });
            
            $('#inventory_menu').addClass('active');
            var rowNum = 0;
            
            //-- start of products grid
            var source = {
                datatype: "json",
                url: "griddata/inventory_list.php",
                id: "id",
                root: "data",
                datafields:
                [
                    { name: 'id', type: 'int' },
                    { name: 'product_id', type: 'string' },
                    { name: 'description', type: 'string' },
                    { name: 'qty_avail', type: 'int' }
                    
                ]
            };

            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                  $('#jqxgrid').jqxGrid('clearselection');
                  loadedRecords = dataAdapter.records;
                  for(i=0; i<loadedRecords.length; i++) {
                    if (loadedRecords[i].active == 'TRUE') {
                      $('#jqxgrid').jqxGrid('selectrow', i);
                    }
                  }
                }
            });

            // initialize jqxGrid
            $("#jqxgrid").jqxGrid(
            {
                width: 1000,
                height: 350,
                source: dataAdapter,
                selectionmode: 'checkbox',
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                editable: true,
                columns: [
                  { text: 'ID', datafield: 'id', hidden: true},
                  { text: 'Product ID', datafield: 'product_id', width: 100, editable: false},
                  { text: 'Description', datafield: 'description', filtertype: 'input', width: 300, editable: false},
                  { text: 'Quantity Available', datafield: 'qty_avail', filtertype: 'number', width: 150, editable: false},
                  { text: 'Quantity to Adjust', datafield: 'qty', filtertype: 'none', width: 150,
                    validation: function (cell, value) {
                          val = $.trim(value);
                          if (val) {
                            if (isNaN(Number(val))) {
                              return { result: false, message: "Please enter a valid number." };
                            } else {
                              valNum = Number(val);
                              if(!(valNum === parseInt(valNum, 10))) {
                                return { result: false, message: "Please enter a whole number." };
                              }
                              
                              qtyAvail = parseInt($('#jqxgrid').jqxGrid('getcellvalue', cell.row, "qty_avail"));
                              if (valNum + qtyAvail < 0) {
                                return { result: false, message: "The available quantity cannot be less than zero." };
                              }
                            }
                          }
                          
                          return true;
                    }},
                  { text: 'Remarks', datafield: 'remarks', filtertype: 'none', width: 300}
                ]
            });
            //-- end of products grid
            
            $("#submit_btn").click(function() {
                $('#errCtnr').hide();
                $('#successCtnr').hide();
                var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0) {
                  var selectedRowData = {data: []};
                  for (i=0; i<getselectedrowindexes.length; i++) {
                    var selectedData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[i]);
                    if (!selectedData.qty) {
                      $('#errCtnr').show();
                      $("#errMsg").text("Please key in a quantity to adjust for the selected products.");
                      return;
                    }
                    selectedRowData.data.push(selectedData);
                  }
                  
                  var tableData = {username: $('#username').html(), button: 'submit' };
                  $.extend(tableData, selectedRowData);
                  $.ajax({
                    method: "POST",
                    url: "fxns/inventory_fxn.php",
                    data: tableData
                  })
                  .done(function( data ) {
                    if(data.has_error == "true") {
                      $('#errCtnr').show();
                      $("#errMsg").text(data.remarks);
                    } else {
                      $('#successCtnr').show();
                      $("#successMsg").text('Inventory updated.');
                      $('#jqxgrid').jqxGrid('updatebounddata');
                    }
                  });
                  /*$.get('fxns/inventory_fxn.php?button=submit', tableData)
                    .done(function(data) {
                            if(data.has_error == "true") {
                              $('#errCtnr').show();
                              $("#errMsg").text(data.remarks);
                            } else {
                              $('#successCtnr').show();
                              $("#successMsg").text('Inventory updated.');
                              $('#jqxgrid').jqxGrid('updatebounddata');
                            }
                          });*/
                } else {
                  $('#errCtnr').show();
                  $("#errMsg").text("Please select at least 1 product.");
                }
            });
            
        });
        
    </script>
  </head>

  <body>

    <?php require("includes/navigation.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="alert alert-success alert-dismissible fade in" role="alert" style="display: none;" id="successCtnr">
              <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <span id="successMsg"></span>
            </div>
            <div class="alert alert-danger alert-dismissible fade in" role="alert" style="display: none;" id="errCtnr">
              <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <span id="errMsg"></span>
            </div>
            <div id="jqxgrid"></div>
            <br />
            <div class="form-group">
                <div class="col-xs-12" style="text-align: center">
                    <button type="button" class="btn btn-primary" id="submit_btn">Submit</button>
                    <button type="submit" class="btn btn-primary">Delete</button>
                    <button type="submit" class="btn btn-primary">New</button>
                </div>
            </div>
        </div>
        

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
