<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();

if (login_check($mysqli) == false) {
  header('Location: login.php?error=login');
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Tori Grill - Sales Entry</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="../scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../scripts/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="../jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="../jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="../jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.pager.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.sort.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.edit.js"></script>  
    <script type="text/javascript" src="../jqwidgets/jqxgrid.selection.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.columnsresize.js"></script>
    
    <script src="../bootstrap/js/bootstrap-datepicker.js"></script>
    <link id="bsdp-css" href="../bootstrap/css/datepicker.css" rel="stylesheet">
    
    <script type="text/javascript">
        $(document).ready(function () {
            var rowNum = 0;
            var selICol; //iCol of selected cell
            var selIRow; //iRow of selected cell
            editIndex = null;
            
            addSalesRow = function() {
              rowNum++;
              var commit = $("#jqxgrid").jqxGrid('addrow', null, {id:"new"+rowNum, menu_id:"", description:"", active: true, price: 0, qty: 1, remarks: "", total: 0});
              
              datainfo = $("#jqxgrid").jqxGrid("getdatainformation");
              rowcount = datainfo.rowscount;
              $("#jqxgrid").jqxGrid("begincelledit", rowcount-1, "menu_id");
              $('#jqxgrid').jqxGrid({ selectedrowindex: rowNum}); 
            }
            
            refreshNestedGrid = function() {
              rowNum++;
              var commit = $("#jqxgrid").jqxGrid('addrow', null, {id:"new"+rowNum, menu_id:"", description:"", active: true, price: 0, qty: 1, remarks: "", total: 0});
              
              datainfo = $("#jqxgrid").jqxGrid("getdatainformation");
              rowcount = datainfo.rowscount;
              $("#jqxgrid").jqxGrid("begincelledit", rowcount-1, "menu_id");
              $('#jqxgrid').jqxGrid({ selectedrowindex: rowNum}); 
            }
            
            //for meal items dropdown
            var mealItemsSource =
            {
                datafields: [
                    { name: 'item_id', type: 'string' },
                    { name: 'item_desc', type: 'string' }
                ],
                datatype: "json",
                url: "datasource/prod_menu_list.php?get=products",
                async: false
            };
            var mealItemsAdapter = new $.jqx.dataAdapter(mealItemsSource, {
                autoBind: true
            });
            
            //grid data - sales lines
            var menuItemsSource =
            {
                datafields: [
                    { name: 'menu_id', type: 'string' },
                    { name: 'product_id', type: 'string' },
                    { name: 'qty', type: 'int' }
                ],
                datatype: "json",
                url: "datasource/prod_menu_list.php?get=salesLines",
                async: false
            };

            var menuItemsDataAdapter = new $.jqx.dataAdapter(menuItemsSource, { autoBind: true });
            menuItems = menuItemsDataAdapter.records;
            var nestedGrids = new Array();
            var nestedGridVals = new Array();

            // create nested grid.
            var initrowdetails = function (index, parentElement, gridElement, record) {
                var menu_id = record.menu_id.toString();
                var id = record.id.toString();
                var grid = $($(parentElement).children()[0]);
                // fill the orders depending on the id.
                var menuItemsById = [];
                
                if (nestedGridVals[index]) {
                  menuItemsById = nestedGridVals[index]
                } else {
                  var filtergroup = new $.jqx.filter();
                  var filter_or_operator = 1;
                  var filtervalue = menu_id;
                  var filterindex;
                  if (id.substring(0,3) == 'new') {
                    filtervalue = menu_id;
                  } else {
                    filtervalue = id;
                  }
                  var filtercondition = 'equal';
                  var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                  for (var m = 0; m < menuItems.length; m++) {
                      var result = filter.evaluate(menuItems[m]["menu_id"]);
                      if (result)
                          menuItemsById.push(menuItems[m]);
                  }
                  nestedGridVals[index] = menuItemsById;
                }
                
                var itemSource = { datafields: [
                    { name: 'menu_id', type: 'string' },
                    { name: 'item', value: 'product_id', values: { source: mealItemsAdapter.records, value: 'item_id', name: 'item_desc' } },
                    { name: 'product_id', type: 'string' },
                    { name: 'qty', type: 'int' }
                ],
                    id: 'menu_id',
                    localdata: menuItemsById,
                    updaterow: function (rowid, newdata, commit) {
                        nestedGridVals[index] = $('#grid'+index).jqxGrid('getboundrows');
                        commit(true);
                    }
                }
                var nestedGridAdapter = new $.jqx.dataAdapter(itemSource);

                if (grid != null) {
                    grid.jqxGrid({
                        source: nestedGridAdapter,
                        width: 550,
                        height: 150,
                        editable: true,//id.substring(0,3) == 'new',
                        columns: [
                          { text: 'Product ID', columntype: 'dropdownlist', datafield: 'product_id', displayfield: 'item',
                            validation: function (cell, value) {
                                if (value == "")
                                    return { result: false, message: "Please fill up this field." };
                                  
                                return true;
                            },
                            createeditor: function (row, value, editor) {
                                editor.jqxDropDownList({ source: mealItemsAdapter, displayMember: 'item_desc', valueMember: 'item_id' });
                            }},
                          //{ text: 'Product ID', datafield: 'product_id', width: 200 },
                          { text: 'Quantity', datafield: 'qty', width: 150 }
                       ],
                      handlekeyboardnavigation: function (event) {
                        var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
                        var thisGrid = $('#grid'+index);
                        var thisRowData;
                        if ((key == 13 || key == 9) && selICol == 'remarks' && selIRow++ == thisGrid.jqxGrid('getdatainformation').rowscount) {
                            var editable = thisGrid.jqxGrid('endcelledit', selIRow, selICol, false);
                            //If at bottom of grid, create new row
                            addSalesRow();
                            return true;
                        } else if (key == 27) {
                          thisRowData = thisGrid.jqxGrid('getrowdata', selIRow);
                          if(thisRowData.menu_id == null || thisRowData.menu_id == '') {
                            var v = thisGrid.jqxGrid('deleterow', 'new'+rowNum);
                            rowNum--;
                          }
                        }
                        return false;
                      }
                    });
                }
                nestedGrids[index] = grid;
            }

            var renderer = function (row, column, value) {
                return '<span style="margin-left: 4px; margin-top: 9px; float: left;">' + value + '</span>';
            }
            
            var productSource = {
                datatype: "json",
                url: "datasource/prod_menu_list.php?get=items",
                datafields:
                [
                    { name: 'item_id', type: 'string' },
                    { name: 'item_desc', type: 'string' },
                    { name: 'item_price', type: 'float' },
                    { name: 'item_type', type: 'string' }
                ],
                async: false
            };
            var productAdapter = new $.jqx.dataAdapter(productSource, {
                autoBind: true
            });
            products = productAdapter.records;
            
            var source = {
                datatype: "json",
                url: "griddata/sales_list.php",
                id: "id",
                root: "data",
                editable: true,
                datafields:
                [
                    { name: 'id', type: 'int' },
                    { name: 'item', value: 'menu_id', values: { source: productAdapter.records, value: 'item_id', name: 'item_desc' } },
                    { name: 'menu_id', type: 'string' },
                    { name: 'description', type: 'string' },
                    { name: 'inventory', type: 'bool' },
                    { name: 'price', type: 'float' },
                    { name: 'qty', type: 'int' },
                    { name: 'total', type: 'float' },
                    { name: 'remarks', type: 'string' }
                ],
                updaterow: function (rowid, newdata, commit) {
                    var prodRec;
                    var itemPrice;
                    var itemType;
                    var itemFilter = newdata.menu_id.toString();
                    for (i=0; i<productAdapter.records.length; i++) {
                      if (productAdapter.records[i].item_id == itemFilter) {
                        itemPrice = productAdapter.records[i].item_price;
                        itemType = productAdapter.records[i].item_type
                        break;
                      }
                    }
                    
                    /*
                     *
                     *
                     var menu_id = record.menu_id.toString();
                var id = record.id.toString();
                var grid = $($(parentElement).children()[0]);
                // fill the orders depending on the id.
                var menuItemsById = [];
                
                if (nestedGridVals[index]) {
                  menuItemsById = nestedGridVals[index]
                } else {
                  var filtergroup = new $.jqx.filter();
                  var filter_or_operator = 1;
                  var filtervalue = menu_id;
                  var filterindex;
                  if (id.substring(0,3) == 'new') {
                    filtervalue = menu_id;
                  } else {
                    filtervalue = id;
                  }
                  var filtercondition = 'equal';
                  var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                  for (var m = 0; m < menuItems.length; m++) {
                      var result = filter.evaluate(menuItems[m]["menu_id"]);
                      if (result)
                          menuItemsById.push(menuItems[m]);
                  }
                  nestedGridVals[index] = menuItemsById;
                }
                     */
                    
                    //console.log($('#jqxgrid').jqxGrid('getrowdatabyid', rowid));
                    
                    if (editField && editField == 'menu_id') {
                      $("#jqxgrid").jqxGrid('setcellvaluebyid', rowid, "price", itemPrice);
                      
                      // synchronize with the server - send update command
                      // call commit with parameter true if the synchronization with the server is successful 
                      // and with parameter false if the synchronization failed.
                      var index = $('#jqxgrid').jqxGrid('getrowboundindexbyid', rowid);
                      var grid = nestedGrids[index];
                      if (grid) {
                        if (itemType == 'P') {
                          nestedGrids[index].jqxGrid('clear');
                        }
                        var filtergroup = new $.jqx.filter();
                        var filtercondition = 'equal';
                        var filter = filtergroup.createfilter('stringfilter', itemFilter, filtercondition);
                        
                        // fill the orders depending on the id.
                        var menuItemsById = [];
                        for (var m = 0; m < menuItems.length; m++) {
                            var result = filter.evaluate(menuItems[m]["menu_id"]);
                            if (result)
                                menuItemsById.push(menuItems[m]);
                        }
                        
                        var itemSource = { datafields: [
                              { name: 'menu_id', type: 'string' },
                              { name: 'item', value: 'product_id', values: { source: mealItemsAdapter.records, value: 'item_id', name: 'item_desc' } },
                              { name: 'product_id', type: 'string' },
                              { name: 'qty', type: 'int' }
                            ],
                            id: 'menu_id',
                            localdata: menuItemsById
                        }
                        var nestedGridAdapter = new $.jqx.dataAdapter(itemSource);
        
                        if (grid != null) {
                            grid.jqxGrid({
                                source: nestedGridAdapter,
                                width: 550,
                                height: 150,
                                editable: true,
                                columns: [
                                  { text: 'Product ID', columntype: 'dropdownlist', datafield: 'product_id', displayfield: 'item',
                                    validation: function (cell, value) {
                                        if (value == "")
                                            return { result: false, message: "Please fill up this field." };
                                          
                                        return true;
                                    },
                                    createeditor: function (row, value, editor) {
                                        editor.jqxDropDownList({ source: mealItemsAdapter, displayMember: 'item_desc', valueMember: 'item_id' });
                                    }},
                                  { text: 'Quantity', datafield: 'qty', width: 150 }
                              ]
                            });
                            
                            $("#grid"+index).on('cellbeginedit', function (event)  {
                                // event arguments.
                                var args = event.args;
                                // column data field.
                                var dataField = event.args.datafield;
                                // row's bound index.
                                var rowBoundIndex = event.args.rowindex;
                                // cell value
                                var value = args.value;
                                // row's data.
                                var rowData = args.row;
                                
                                console.log('edit ' + dataField);
                                
                                selICol = dataField;
                                selIRow = rowBoundIndex;
                                editIndex = rowBoundIndex;
                              });
                        }
                        nestedGrids[index] = grid;
                        $('#grid'+index).jqxGrid('updatebounddata');
                      }
                    }
                    
                    if (editField && editField != 'remarks' && editField != 'total') {
                      rowTotal = parseFloat(newdata.price) * parseInt(newdata.qty);
                      $("#jqxgrid").jqxGrid('setcellvaluebyid', rowid, "total", rowTotal);
                    }
                    
                    //console.log(newdata);
                    
                    commit(true);
                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $("#jqxgrid").jqxGrid(
            {
                width: 850,
                height: 350,
                source: dataAdapter,
                editable: true,
                selectionmode: 'checkbox',
                editmode: 'click',
                showtoolbar: true,
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                rowdetails: true,
                //rowsheight: 35,
                initrowdetails: initrowdetails,
                rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 10px;'></div>", rowdetailshidden: true },
                /*ready: function () {
                    $("#jqxgrid").jqxGrid('showrowdetails', 0);
                },*/
                handlekeyboardnavigation: function (event) {
                  var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
                  var thisGrid = $('#jqxgrid');
                  var thisRowData;
                  if ((key == 13 || key == 9) && selICol == 'remarks' && selIRow++ == thisGrid.jqxGrid('getdatainformation').rowscount) {
                      var editable = thisGrid.jqxGrid('endcelledit', selIRow, selICol, false);
                      //If at bottom of grid, create new row
                      addSalesRow();
                      return true;
                  } else if (key == 27) {
                    thisRowData = thisGrid.jqxGrid('getrowdata', selIRow);
                    if(thisRowData.menu_id == null || thisRowData.menu_id == '') {
                      var v = thisGrid.jqxGrid('deleterow', 'new'+rowNum);
                      rowNum--;
                    }
                  }
                  return false;
                },
                columns: [
                  { text: 'ID', datafield: 'id', columntype: 'textbox', hidden: true, editable: false },
                  { text: 'Menu ID', columntype: 'dropdownlist', datafield: 'menu_id', displayfield: 'item',
                    validation: function (cell, value) {
                        if (value == "")
                            return { result: false, message: "Please fill up this field." };
                          
                        return true;
                    },
                    createeditor: function (row, value, editor) {
                        editor.jqxDropDownList({ source: productAdapter, displayMember: 'item_desc', valueMember: 'item_id' });
                    }},
                  { text: 'Price', datafield: 'price', columntype: 'textbox', width: 100, cellsformat: 'd2', cellsalign: 'right',
                    validation: function (cell, value) {
                          val = $.trim(value);
                          if (Number(val) < 0) {
                            return { result: false, message: "Price cannot be less than 0." };
                          }
                          return true;
                    }},
                  { text: 'Quantity', datafield: 'qty', columntype: 'textbox', width: 100, cellsalign: 'right',
                    validation: function (cell, value) {
                          val = $.trim(value);
                          if (val) {
                            if (isNaN(Number(val))) {
                              return { result: false, message: "Please enter a valid number." };
                            } else {
                              valNum = Number(val);
                              if(!(valNum === parseInt(valNum, 10))) {
                                return { result: false, message: "Please enter a whole number." };
                              }
                              
                              if (valNum <= 0) {
                                return { result: false, message: "Quantity cannot be less than 1." };
                              }
                            }
                          }
                          
                          return true;
                    }},
                  { text: 'Total', datafield: 'total', columntype: 'textbox', width: 100, cellsformat: 'd2', cellsalign: 'right',
                    validation: function (cell, value) {
                          val = $.trim(value);
                          if (Number(val) < 0) {
                            return { result: false, message: "Total cannot be less than 0." };
                          }
                          return true;
                    }},
                  { text: 'Remarks', width: 300, datafield: 'remarks'}
                ],
                rendertoolbar: function (toolbar) {
                    var me = this;
                    var container = $("<div style='margin: 5px;'></div>");
                    toolbar.append(container);
                    container.append('<input class="btn btn-inverse" id="addrowbutton" type="button" value="Add New Row" />');
                    container.append('<input class="btn btn-inverse" id="addmealrowbutton" type="button" value="Add Meal Item" />');
                    $("#addrowbutton").jqxButton();
                    $("#addmealrowbutton").jqxButton();
                    //$("#deleterowbutton").jqxButton();
                    //$("#updaterowbutton").jqxButton();
                    //$("#saverows").jqxButton();
                    // update row.
                    //$("#updaterowbutton").on('click', function () {
                    //    var datarow = generaterow();
                    //    var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                    //    var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                    //    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    //        var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    //        var commit = $("#jqxgrid").jqxGrid('updaterow', id, datarow);
                    //        $("#jqxgrid").jqxGrid('ensurerowvisible', selectedrowindex);
                    //    }
                    //});

                    // create new row.
                    $("#addrowbutton").on('click', function () {
                      addSalesRow();
                    });

                    // create new meal item.
                    $("#addmealrowbutton").on('click', function () {
                      if (editIndex != null && editIndex >= 0) {
                        var editRowData = $('#jqxgrid').jqxGrid('getrowdata', editIndex);
                        if (editRowData['menu_id'] == null || editRowData['menu_id'] == '') {
                          $('#errCtnr').show();
                          $("#errMsg").text("Please select a Menu ID.");
                          return;
                        }
                        
                        for (var m = 0; m < products.length; m++) {
                          if (editRowData['menu_id'] == products[m]['item_id'] && products[m]['item_type'] == 'P') {
                            $('#errCtnr').show();
                            $("#errMsg").text("Menu Items cannot be added to Products.");
                            return;
                          }
                        }
                        
                        $('#jqxgrid').jqxGrid('showrowdetails', editIndex);
                        var commit = nestedGrids[editIndex].jqxGrid('addrow', null, {menu_id:"", product_id:"", qty: 1});
                        datainfo = nestedGrids[editIndex].jqxGrid("getdatainformation");
                        rowcount = datainfo.rowscount;
                        nestedGrids[editIndex].jqxGrid("begincelledit", rowcount-1, "product_id");
                      } else {
                        $('#errCtnr').show();
                        $("#errMsg").text("Please select a row.");
                      }
                        
                        /*rowNum++;
                        var commit = $("#jqxgrid").jqxGrid('addrow', null, {id:"new"+rowNum, menu_id:"", description:"", active: true, price: 0, qty: 1, remarks: "", total: 0});
                        
                        datainfo = $("#jqxgrid").jqxGrid("getdatainformation");
                        rowcount = datainfo.rowscount;
                        $("#jqxgrid").jqxGrid("begincelledit", rowcount-1, "menu_id");*/
                    });

                    // delete row.
                    //$("#deleterowbutton").on('click', function () {
                    //    var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                    //    var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                    //    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    //        var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    //        var commit = $("#jqxgrid").jqxGrid('deleterow', id);
                    //    }
                    //});
                }
            });
          
          $("#jqxgrid").on('cellbeginedit', function (event)  {
            // event arguments.
            var args = event.args;
            // column data field.
            var dataField = event.args.datafield;
            // row's bound index.
            var rowBoundIndex = event.args.rowindex;
            // cell value
            var value = args.value;
            // row's data.
            var rowData = args.row;
            
            selICol = dataField;
            selIRow = rowBoundIndex;
            editIndex = rowBoundIndex;
          });
          
          $("#jqxgrid").on('cellendedit', function (event) {
            // event arguments.
            var args = event.args;
            // column data field.
            var dataField = event.args.datafield;
            // row's bound index.
            var rowBoundIndex = event.args.rowindex;
            // cell value
            var value = args.value;
            // cell old value.
            var oldvalue = args.oldvalue;
            // row's data.
            var rowData = args.row;
            
            selICol = null;
            selIRow = null;
            editIndex = null;
            editField = dataField;
          });
          
          $('#trans_date').datepicker({
            autoclose: true,
            todayHighlight: true
          });
          
          $("#trans_date").datepicker("setDate", new Date());
          $("#trans_date").datepicker('update');
          
          $('.alert .close').on('click', function(e) {
                $(this).parent().hide();
            });
          
          $("#submit_btn").click(function() {
                $('#errCtnr').hide();
                $('#successCtnr').hide();
                var selectedrowindex;
                var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                if (getselectedrowindexes.length > 0) {
                  var selectedRowData = {data: []};
                  for (var i=0; i<getselectedrowindexes.length; i++) {
                    selectedrowindex = getselectedrowindexes[i];
                    var detailRowsData = {details: []};
                    
                    var selectedData = $('#jqxgrid').jqxGrid('getrowdata', selectedrowindex);
                    var detailRows = nestedGrids[selectedrowindex].jqxGrid('getrows');
                    for (var j=0; j<detailRows.length; j++) {
                      detailRowsData.details.push(detailRows[j]);
                    }
                    $.extend(selectedData, detailRowsData );
                    
                    selectedRowData.data.push(selectedData);
                  }
                  
                  $.get('fxns/inventory_fxn.php?button=submit', selectedRowData)
                    .done(function(data) {
                            if(data.has_error == "true") {
                              $('#errCtnr').show();
                              $("#errMsg").text(data.remarks);
                            } else {
                              $('#successCtnr').show();
                              $("#successMsg").text('Inventory updated.');
                              $('#jqxgrid').jqxGrid('updatebounddata');
                            }
                          });
                } else {
                  $('#errCtnr').show();
                  $("#errMsg").text("Please select at least 1 row.");
                }
            });
        });
    </script>
  </head>

  <body>

    <?php require("includes/navigation.php"); ?>

    <div class="container">
        <div class="starter-template">
            <form class="form-horizontal" id="main_form">
                <div class="alert alert-success alert-dismissible fade in" role="alert" style="display: none;" id="successCtnr">
                  <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <span id="successMsg"></span>
                </div>
                <div class="alert alert-danger alert-dismissible fade in" role="alert" style="display: none;" id="errCtnr">
                  <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <span id="errMsg"></span>
                </div>
                <div class="form-group">
                    <label for="menu_id" class="control-label col-xs-2">Transaction Date</label>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" name="trans_date" id="trans_date" placeholder="Transaction Date" maxlength=10 width="50px">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="control-label col-xs-2">Menu Items</label>
                    <div class="col-xs-10">
                        <div id="jqxgrid"></div>
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <div class="col-xs-12" style="text-align: center">
                        <button type="button" class="btn btn-primary" id="submit_btn">Submit</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                        <button type="submit" class="btn btn-primary">New</button>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" />
            </form>
        </div>
        

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
