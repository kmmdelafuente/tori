<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
extract($_GET);
$trans_date = new DateTime($trans_date);

/**
 * - Check if product is inventory item.
 * - If yes, adjust inventory.
 * - If no, proceed with update.
 **/
$row_result = [];
if($button == 'submit' || $button == 'delete') {
    foreach ($data as $value) {
        try {
            $sales_qty = 0;
            $sales_id = 0;
            $sales_qty_to_adj = 0;
            if (substr($value['id'], 0, 3) != 'new') {
                $sales_id = $value['id'];
                $stmt = $mysqli->prepare("SELECT * FROM sales WHERE seq_no = ?");
                
                if (!$stmt->bind_param('i', $sales_id)) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Bind failed: (' . $stmt->errno . ') ' . $stmt->error);
                    continue;
                }
                
                if (!$stmt->execute()) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error);
                    continue;
                }
                if (!($res = $stmt->get_result())) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Getting result set failed: (' . $stmt->errno . ') ' . $stmt->error);
                    continue;
                }
                
                $sales_rec = $res->fetch_assoc();
                $sales_qty = $sales_rec['qty'];
                
                $sales_qty_to_adj = $qty - $sales_qty;
                if ($sales_qty_to_adj == 0) {
                    $row_result[$value['id']] = array('success' => 'true');
                    continue;
                }
            }
            
            if ($value['qty'] <= 0) {
                $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Quantity cannot be less than 1.');
                continue;
            }
            
            $mysqli->autocommit(false);
            $product_items = [];
            $total_qty = 0;
            if ($value['item_type'] == 'M') {
                foreach($value['details'] as $product_item) {
                    $previous_qty = 0;
                    if(isset($product_items[$product_item['product_id']])) {
                        $previous_qty = $product_items[$product_item['product_id']];
                    }
                    $product_items[$product_item['product_id']] = $product_item['qty'] + $previous_qty;
                    $total_qty += $product_item['qty'];
                }
            
                if ($total_qty <= 0) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Total meal items cannot be less than 1.');
                    continue;
                    
                }
            } else {
                $product_items[$value['menu_id']] = $value['qty'];
            }
            
            $stmt = $mysqli->prepare("SELECT * FROM product WHERE product_id = ?");
            foreach ($product_items as $key => $val) {
                if($val == 0) {
                    continue;
                }
                
                if (!$stmt->bind_param('s', $key)) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Bind failed: (' . $stmt->errno . ') ' . $stmt->error);
                    continue 2;
                }
                
                if (!$stmt->execute()) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error);
                    continue 2;
                }
                if (!($res = $stmt->get_result())) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Getting result set failed: (' . $stmt->errno . ') ' . $stmt->error);
                    continue 2;
                }
                
                if($res['inventory'] == 'TRUE' && (
                        ($sales_id == 0 && ($res['qty'] - ($qty * $val)) >= 0) ||
                        ($sales_id > 0 && $sales_qty_to_adj > 0 && ($res['qty'] - ($sales_qty_to_adj * $val)) >= 0))) {
                    $row_result[$value['id']] = array('success' => 'false', 'remarks' => 'Quantity on hand for ' . $res['description'] . ' is not enough to proceed.');
                    continue 2;
                }
                
                print_r($res->fetch_assoc());
            }
            /*$remarks = empty($value['remarks']) ? null : $value['remarks'];
            $qtyAvail = $value['qty_avail'];
            $adjQty = $value['qty'];
            $bfQoh = 0;
            $moveQty = 0;
            
            $result = mysqli_query($mysqli,"SELECT * FROM invt_movement where product_id='" . $value['product_id'] . "' order by seq_no desc");
            if (!$result) {
                printf("Error: %s\n", mysqli_error($mysqli));
                exit();
            }
            
            while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
                $bfQoh = $row['bf_qoh'];
                $moveQty = $row['qty'];
                break;
            }*/
            $mysqli->commit();
            $row_result[$value['id']] = array('new_id' => 1, 'success' => 'true');
        } catch(Exception $e) {
            $mysqli->rollback;
            $row_result[$value['id']] = array('success' => 'false', 'error' => $e->getMessage());
        }
        
    }
        
    echo json_encode(array('success' => 'true', 'has_error' => 'false', 'sys_remarks' => $row_result));
    return;
}

?>