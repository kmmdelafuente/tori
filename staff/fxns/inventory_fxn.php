<?php
include_once '../includes/db_connect.php';
include_once 'adjust_inventory_fxn.php';

header("Content-type: application/json"); 
extract($_POST);


if($button == 'submit') {
    foreach ($data as $value) {
        $remarks = empty($value['remarks']) ? null : $value['remarks'];
//        $result = adjustInventory($mysqli, $value['product_id'], 'U', $remarks, $value['qty'], $username);
//        if ($result) {
//            $mysqli->rollback();
//            echo json_encode($result);
//            return;
//        }
//        
//        $mysqli->rollback();
        $qtyAvail = $value['qty_avail'];
        $adjQty = $value['qty'];
        $bfQoh = 0;
        $moveQty = 0;
        
        $result = mysqli_query($mysqli,"SELECT * FROM invt_movement where product_id='" . $value['product_id'] . "' order by seq_no desc");
        if (!$result) {
            printf("Error: %s\n", mysqli_error($mysqli));
            exit();
        }
        
        while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
            $bfQoh = $row['bf_qoh'];
            $moveQty = $row['qty'];
            break;
        }
        
        if ($bfQoh + $moveQty <> $qtyAvail) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Movement quantity not sync. Contact admin.'));
            return;
        }
        
        $sql = "INSERT INTO `invt_movement` (`product_id`,`bf_qoh`,`qty`,`trans_type`,`trans_date`,`remarks`) " .
                "VALUES ('".$value['product_id']."',".$qtyAvail.",".$adjQty.",'U',now(),'" . $remarks . "')";
        
        if (!mysqli_query($mysqli, $sql)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Insert movement error: ' . mysqli_error($mysqli) . ' ' . $sql));
            return;
        }
        
        $update_query = "UPDATE `product` set `qty` = " . ($adjQty + $qtyAvail) . " WHERE id = " . $value['id'] ;
        if (!mysqli_query($mysqli, $update_query)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Update product error: ' . mysqli_error($mysqli) . ' ' . $update_query));
            return;
        }
    }
    unset($value);
        
    echo json_encode(array('success' => 'true', 'has_error' => 'false'));
    return;
}
