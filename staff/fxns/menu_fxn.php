<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
extract($_GET);

if(!isset($active)) $active = 'FALSE';

if($button == 'submit') {
    if(empty($id)) {
        $sql = "INSERT INTO `menu` (`menu_id`, `description`, `active`, `price`)" .
                "VALUES ('".$menu_id."','".$description."','".$active."',".$price.")";
        
        if (!mysqli_query($mysqli, $sql)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Insert error: ' . mysqli_error($mysqli)));
            return;
        }
        
        $id = mysqli_insert_id($mysqli);
    } else {
        $sql = "UPDATE `menu` set `description` = '" . $description . "', " .
                                  "`active` = '" . $active . "', " .
                                  "`price` = " . $price . " " .
                "WHERE id = " . $id;
        if (!mysqli_query($mysqli, $sql)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Update error: ' . mysqli_error($mysqli) . ' ' . $sql));
            return;
        }
        
        // sql to delete a record
        $sql = "DELETE FROM `menu_items` WHERE `menu_id`= '" . $menu_id . "'";
        if (!mysqli_query($mysqli, $sql)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => mysqli_error($mysqli) /*. ' ' . $update_query*/));
            return;
        }
    }
    
    
    foreach ($data as $value) {
        $sql = "INSERT INTO `menu_items` (`menu_id`, `product_id`,`qty`) " .
                "VALUES ('".$menu_id."','".$value['product_id']."',".$value['qty'].")";
        
        if (!mysqli_query($mysqli, $sql)) {
            echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Insert items error: ' . mysqli_error($mysqli) . ' ' . $sql));
            return;
        }
    }
    unset($value);
        
    echo json_encode(array('success' => 'true', 'has_error' => 'false', 'id' => $id));
    return;
}

?>