<?php

function adjustInventory($mysqli, $product_id, $trans_type, $remarks, $qty_avail, $adj_qty, $username) {
    $bfQoh = 0;
    $moveQty = 0;

    $result = mysqli_query($mysqli,"SELECT * FROM invt_movement where product_id='" . $product_id . "' order by seq_no desc");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }

    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
        $bfQoh = $row['bf_qoh'];
        $moveQty = $row['qty'];
        break;
    }

    if ($bfQoh + $moveQty <> $qty_avail) {
        echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Movement quantity not sync. Contact admin.'));
        return;
    }

    $sql = "INSERT INTO `invt_movement` (`product_id`,`bf_qoh`,`qty`,`trans_type`,`trans_date`,`remarks`, `log_name`) " .
            "VALUES ('".$product_id."',".$qty_avail.",".$adj_qty.",'" . $trans_type . "',now(),'" . $remarks . "', '" . $username . "')";

    if (!mysqli_query($mysqli, $sql)) {
        echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Insert movement error: ' . mysqli_error($mysqli) . ' ' . $sql));
        return;
    }

    $update_query = "UPDATE `product` set `qty` = " . ($adj_qty + $qty_avail) . " WHERE id = " . $product_id ;
    if (!mysqli_query($mysqli, $update_query)) {
        echo json_encode(array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Update product error: ' . mysqli_error($mysqli) . ' ' . $update_query));
        return;
    }
}

function adjustInventoryxx($mysqli, $product_id, $trans_type, $remarks, $adj_qty, $username) {
    $mysqli->autocommit(FALSE);
    $bf_qoh = 0;
    $move_qty = 0;
    $qty_avail = 0;
    
    /**
     * get latest movement record of the product
     */
    $stmt = $mysqli->prepare("SELECT * FROM invt_movement where product_id = ? order by seq_no desc");
    if (!$stmt->bind_param('s', $product_id)) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Bind failed: (' . $stmt->errno . ') ' . $stmt->error . ' --movement.');
    }
    if (!$stmt->execute()) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error . ' --movement.');
    }
    if (!($res = $stmt->get_result())) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Getting result set failed: (' . $stmt->errno . ') ' . $stmt->error . ' --movement.');
    }
    while($row = $res->fetch_assoc()) {
        $bf_qoh = $row['bf_qoh'];
        $move_qty = $row['qty'];
        break;
    }
    
    /**
     * get quantity on hand of the product
     */
    $stmt = $mysqli->prepare("SELECT * FROM product where product_id = ?");
    if (!$stmt->bind_param('s', $product_id)) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Bind failed: (' . $stmt->errno . ') ' . $stmt->error . ' QoH.');
    }
    if (!$stmt->execute()) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error . ' QoH.');
    }
    if (!($res = $stmt->get_result())) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Getting result set failed: (' . $stmt->errno . ') ' . $stmt->error . ' QoH.');
    }
    while($row = $res->fetch_assoc()) {
        $qty_avail = $row['qty'];
        break;
    }
    
    // check if movement record is consistent with the quantity on hand
    if ($bf_qoh + $move_qty <> $qty_avail) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Movement quantity not sync. Contact admin.');
    }
    
    // if everything is OK, proceed to adjust the inventory
    $stmt = $mysqli->prepare("INSERT INTO invt_movement (product_id, bf_qoh, qty, trans_type, remarks, log_name) " .
                             "VALUES (?, ?, ?, ?, ?, ?)");
    if (!$stmt->bind_param('siisss', $product_id, $qty_avail, $adj_qty, $trans_type, $remarks, $username)) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Bind failed: (' . $stmt->errno . ') ' . $stmt->error . ' --insert movement.');
    }
    if (!$stmt->execute()) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error . ' --insert movement.');
    }
    
    echo 1;
    $stmt = $mysqli->prepare("UPDATE product SET qty = ? where product_id = ?");
    echo 2;
    $hasError = $stmt->bind_param('is', ($adj_qty + $qty_avail), $product_id);
    echo 21;
    if (false === $hasError) {
        echo 22;
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Bind failed: (' . $stmt->errno . ') ' . $stmt->error . ' --update QoH.');
    }
    echo 3;
    $hasError = $stmt->execute();
    if (false === $hasError) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Execute failed: (' . $stmt->errno . ') ' . $stmt->error . ' --update QoH.');
    }
    echo 4;
    $hasError = $mysqli->commit();
    if (false === $hasError) {
        return array('success' => 'true', 'has_error' => 'true', 'remarks' => 'Transaction failed to commit.');
    }
    echo 5;
    
    return null;
}

?>