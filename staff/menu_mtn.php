<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();

if (login_check($mysqli) == false) {
  header('Location: login.php?error=login');
}

$username = get_user($mysqli);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Tori Grill - Product Maintenance</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="../scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../scripts/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="../jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="../jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="../jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.pager.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.sort.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.edit.js"></script>  
    <script type="text/javascript" src="../jqwidgets/jqxgrid.selection.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.columnsresize.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            var setInputFields = function(id, menuId, desc, active, price) {
              $("#menu_id").val(menuId);
              $("#id").val(id);
              $("#description").val(desc);
              $("#price").val(price);
              $('#active').prop('checked', active == 'TRUE');
            };
            
            $('#products_menu').addClass('active');
            var rowNum = 0;
            
            //-- start of products grid
            var source = {
                datatype: "json",
                url: "griddata/menu_child_list.php",
                id: "id",
                root: "data",
                data: {
                    menuId: function() { return $("#menu_id").val(); }
                },
                datafields:
                [
                    { name: 'id', type: 'int' },
                    { name: 'product_id', type: 'string' },
                    { name: 'description', type: 'string' },
                    { name: 'qty', type: 'int' },
                    { name: 'active', type: 'string' }
                    
                ],
                addrow: function (rowid, rowdata, position, commit) {
                    // synchronize with the server - send insert command
                    // call commit with parameter true if the synchronization with the server is successful 
                    //and with parameter false if the synchronization failed.
                    // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                    commit(true);
                },
                deleterow: function (rowid, commit) {
                    // synchronize with the server - send delete command
                    // call commit with parameter true if the synchronization with the server is successful 
                    //and with parameter false if the synchronization failed.
                    commit(true);
                },
                updaterow: function (rowid, newdata, commit) {
                    // synchronize with the server - send update command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failed.
                    commit(true);
                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                  $('#jqxgrid').jqxGrid('clearselection');
                  loadedRecords = dataAdapter.records;
                  for(i=0; i<loadedRecords.length; i++) {
                    if (loadedRecords[i].active == 'TRUE') {
                      $('#jqxgrid').jqxGrid('selectrow', i);
                    }
                  }
                }
            });

            // initialize jqxGrid
            $("#jqxgrid").jqxGrid(
            {
                width: 680,
                height: 350,
                source: dataAdapter,
                selectionmode: 'checkbox',
                editmode: 'click',
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                editable: true,
                columns: [
                  { text: 'ID', datafield: 'id', hidden: true},
                  { text: 'Product ID', datafield: 'product_id', width: 100, editable: false},
                  { text: 'Description', datafield: 'description', filtertype: 'input', width: 400, editable: false},
                  { text: 'Quantity', datafield: 'qty', columntype: 'textbox', width: 100, editable: true,
                    validation: function (cell, value) {
                          val = $.trim(value);
                          if (val) {
                            if (isNaN(Number(val))) {
                              return { result: false, message: "Please enter a valid number." };
                            } else {
                              valNum = Number(val);
                              if(!(valNum === parseInt(valNum, 10))) {
                                return { result: false, message: "Please enter a whole number." };
                              }
                              
                              if (valNum <= 0) {
                                return { result: false, message: "Quantity cannot be less than 1." };
                              }
                            }
                          }
                          
                          return true;
                    }},
                  { text: 'Selected', datafield: 'active', hidden: true}
                ]
            });
            //-- end of products grid
            
            //-- start of menu browse grid
            var menuSource = {
                datatype: "json",
                url: "griddata/menu_browse_list.php",
                id: "id",
                root: "data"
            };

            var menuDataAdapter = new $.jqx.dataAdapter(menuSource);

            // initialize jqxGrid
            $("#menuGrid").jqxGrid(
            {
                width: 500,
                height: 350,
                source: menuDataAdapter,
                selectionmode: 'singlerow',
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                columns: [
                  { text: 'ID', datafield: 'id', hidden: true, editable: false },
                  { text: 'Menu ID', datafield: 'menu_id', width: 100, editable: false },
                  { text: 'Description', datafield: 'description', filtertype: 'input', width: 400, editable: false }
                ]
            });
            
            $('#menuGrid').on('rowselect', function (event) {
              //alert("Row with bound index: " + event.args.rowindex + " has been selected");
              selVal = event.args.row;
              $("#myModal").modal('hide');
              setInputFields(selVal.id, selVal.menu_id, selVal.description, selVal.active, selVal.price)
              $('#jqxgrid').jqxGrid('updatebounddata');
            });
            //-- end of menu browse grid
            
            $("#main_form").validate({
              rules: {
                      menu_id: "required",
                      description: {
                              required: true,
                              minlength: 5
                      },
                      price: {
                              required: true,
                              number: true
                      }
              },
              messages: {
                      description: {
                              minlength: "Your description must consist of at least 5 characters"
                      }
              }
            });
            
            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
            
            mainForm = $("#main_form");
            mainForm.validate();
            $("#submit_btn").click(function() {
                if (mainForm.valid()) {
                  $('#errCtnr').hide();
                  $('#successCtnr').hide();
                  var getselectedrowindexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
                  if (getselectedrowindexes.length > 0) {
                    var selectedRowData = {data: []};
                    for (i=0; i<getselectedrowindexes.length; i++) {
                      var selectedData = $('#jqxgrid').jqxGrid('getrowdata', getselectedrowindexes[i]);
                      if (!selectedData.qty) {
                        $('#errCtnr').show();
                        $("#errMsg").text("Please key in a quantity for the selected products.");
                        return;
                      }
                      selectedRowData.data.push(selectedData);
                    }
                    
                    var formVal = $('#main_form').serializeObject();
                    $.extend(true, formVal, selectedRowData);
                    
                    $.get('fxns/menu_fxn.php?button=submit', formVal)
                      .done(function(data) {
                              if(data.has_error == "true") {
                                $('#errCtnr').show();
                                $("#errMsg").text(data.remarks);
                              } else {
                                $('#successCtnr').show();
                                $("#successMsg").text('Record saved.');
                                $("#id").val(data.id);
                                $('#menuGrid').jqxGrid('updatebounddata');
                              }
                            });
                  } else {
                    $('#errCtnr').show();
                    $("#errMsg").text("Please select at least 1 child product.");
                  }
                }
            });
            
            $("#browse_btn").click(function() {
                $("#myModal").modal('show');
            });
            
            $('.alert .close').on('click', function(e) {
                $(this).parent().hide();
            });
        });
        
    </script>
  </head>

  <body>

    <?php require("includes/navigation.php"); ?>

    <div class="container">
        <div class="starter-template">
            <form class="form-horizontal" id="main_form">
                <div class="alert alert-success alert-dismissible fade in" role="alert" style="display: none;" id="successCtnr">
                  <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <span id="successMsg"></span>
                </div>
                <div class="alert alert-danger alert-dismissible fade in" role="alert" style="display: none;" id="errCtnr">
                  <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <span id="errMsg"></span>
                </div>
                <div class="form-group">
                    <label for="menu_id" class="control-label col-xs-2">Menu ID</label>
                    <div class="col-xs-7">
                      <input type="text" class="form-control" name="menu_id" id="menu_id" placeholder="Menu ID" maxlength=20 width="50px">
                      <button type="button" class="btn btn-primary" id="browse_btn">Browse</button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="control-label col-xs-2">Description</label>
                    <div class="col-xs-7">
                        <input type="text" class="form-control" name="description" id="description" placeholder="Description" maxlength=100>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="control-label col-xs-2">Price</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control" name="price" id="price" placeholder="Price" maxlength=7>
                    </div>
                </div>
                <div class="form-group">
                    <label for="active" class="control-label col-xs-2">Active</label>
                    <div class="col-xs-3">
                        <input type="checkbox" name="active" id="active" value="TRUE">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="control-label col-xs-2">Menu Items</label>
                    <div class="col-xs-10">
                        <div id="jqxgrid"></div>
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <div class="col-xs-12" style="text-align: center">
                        <button type="button" class="btn btn-primary" id="submit_btn">Submit</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                        <button type="submit" class="btn btn-primary">New</button>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" />
            </form>
        </div>
        

    </div><!-- /.container -->
    
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Browse Menu Items</h4>
                </div>
                <div class="modal-body">
                    <div id="menuGrid"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
