<?php
include_once '../includes/db_connect.php';

header("Content-type: application/json"); 
$data = array();
extract($_GET);

if($get == 'items') {
    $result = mysqli_query($mysqli,"SELECT `menu_id`, `description`, `price` " .
                                   "FROM `menu` where `active` = 'TRUE' order by `description`");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
    
    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
      $data[] = array('item_id' => $row['menu_id'],
                      'item_desc' => $row['description'],
                      'item_price' => $row['price'],
                      'item_type' => 'M');
    }
    
    unset($result);
    $result = mysqli_query($mysqli,"SELECT `product_id`, `description`, `price` " .
                                "FROM `product` where `active` = 'TRUE' order by `description`");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
    
    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
      $data[] = array('item_id' => $row['product_id'],
                      'item_desc' => $row['description'],
                      'item_price' => $row['price'],
                      'item_type' => 'P');
    }
}

if($get == 'items' || $get == 'products') {
    unset($result);
    $result = mysqli_query($mysqli,"SELECT `product_id`, `description`, `price` " .
                                "FROM `product` where `active` = 'TRUE' order by `description`");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
    
    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
      $data[] = array('item_id' => $row['product_id'],
                      'item_desc' => $row['description'],
                      'item_price' => $row['price'],
                      'item_type' => 'P');
    }
}

if($get == 'menuItems') {
    $result = mysqli_query($mysqli,"SELECT * FROM menu_items where menu_id = ' " . $menu_id . "' order by menu_id");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
    
    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
      $data[] = array('product_id' => $row['product_id'],
                      'qty' => $row['qty']);
    }
}

if($get == 'salesLines') {
    $result = mysqli_query($mysqli,"SELECT * FROM sales_d order by sales_seq, product_id");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
    
    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
      $data[] = array('product_id' => $row['product_id'],
                      'sales_id' => $row['sales_seq'],
                      'menu_id' => null,
                      'qty' => $row['qty']);
    }
    
    $result = mysqli_query($mysqli,"SELECT * FROM menu_items order by menu_id, product_id");
    if (!$result) {
        printf("Error: %s\n", mysqli_error($mysqli));
        exit();
    }
    
    while($row = mysqli_fetch_array($result, MYSQL_ASSOC)) {
      $data[] = array('product_id' => $row['product_id'],
                      'sales_id' => null,
                      'menu_id' => $row['menu_id'],
                      'qty' => $row['qty']);
    }
}

echo "{\"data\":" .json_encode($data). "}";
?>