<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();

if (login_check($mysqli) == false) {
  header('Location: login.php?error=login');
}

$username = get_user($mysqli);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Tori Grill - Product Maintenance</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="../scripts/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="../jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="../jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="../jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.pager.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.sort.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.edit.js"></script>  
    <script type="text/javascript" src="../jqwidgets/jqxgrid.selection.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="../jqwidgets/jqxgrid.columnsresize.js"></script> 
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#products_menu').addClass('active');
            var rowNum = 0;
            
            var source = {
                datatype: "json",
                url: "griddata/product_list.php",
                id: "id",
                root: "data",
                editable: true,
                datafields:
                [
                    { name: 'id', type: 'int' },
                    { name: 'product_id', type: 'string' },
                    { name: 'description', type: 'string' },
                    { name: 'inventory', type: 'bool' },
                    { name: 'price', type: 'float' },
                    { name: 'active', type: 'bool' }
                ],
                addrow: function (rowid, rowdata, position, commit) {
                    // synchronize with the server - send insert command
                    // call commit with parameter true if the synchronization with the server is successful 
                    //and with parameter false if the synchronization failed.
                    // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                    commit(true);
                },
                deleterow: function (rowid, commit) {
                    // synchronize with the server - send delete command
                    // call commit with parameter true if the synchronization with the server is successful 
                    //and with parameter false if the synchronization failed.
                    commit(true);
                },
                updaterow: function (rowid, newdata, commit) {
                    // synchronize with the server - send update command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failed.
                    if (newdata.product_id && newdata.description) {
                      var data = "update=true&" + $.param(newdata);
                      $.ajax({
                        dataType: 'json',
                        url: 'griddata/product_list.php',
                        cache: false,
                        data: data,
                        success: function (data, status, xhr) {
                          if (data.has_error == 'true') {
                            $("#jqxgrid").jqxGrid('setcellvaluebyid', rowid, "remarks", data.remarks);
                          } else {
                            $("#jqxgrid").jqxGrid('setcellvaluebyid', rowid, "id", data.id);
                            $("#jqxgrid").jqxGrid('setcellvaluebyid', rowid, "remarks", "");
                          }
                          commit(true);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                          commit(false);
                        }
                      });
                    }
                    commit(true);
                },
                pager: function (pagenum, pagesize, oldpagenum) {
                    // callback called when a page or page size is changed.
                }
            };

            var dataAdapter = new $.jqx.dataAdapter(source);

            // initialize jqxGrid
            $("#jqxgrid").jqxGrid(
            {
                width: 850,
                height: 350,
                source: dataAdapter,
                editable: true,
                selectionmode: 'singlecell',
                editmode: 'dblclick',
                showtoolbar: true,
                sortable: true,
                pageable: true,
                showfilterrow: true,
                filterable: true,
                columns: [
                  { text: 'ID', datafield: 'id', columntype: 'textbox', width: 0, hidden: true, editable: false },
                  { text: 'Product ID', columntype: 'textbox', datafield: 'product_id', width: 100,
                    validation: function (cell, value) {
                          if (value == "")
                            return { result: false, message: "Please fill up this field." };
                          
                          return true;
                    }},
                  { text: 'Description', datafield: 'description', filtertype: 'input', columntype: 'textbox', width: 400,
                    validation: function (cell, value) {
                          if (value == "")
                            return { result: false, message: "Please fill up this field." };
                          
                          return true;
                    }},
                  { text: 'Inventory Item', datafield: 'inventory', filtertype: 'bool', columntype: 'checkbox', width: 50},
                  { text: 'Price', datafield: 'price', columntype: 'textbox', width: 100, cellsformat: 'd2', cellsalign: 'right' },
                  { text: 'Active', datafield: 'active', filtertype: 'bool', columntype: 'checkbox', width: 50},
                  { text: 'Remarks', width: 300, datafield: 'remarks', filtertype: 'none', editable: false},
                ],
                rendertoolbar: function (toolbar) {
                    var me = this;
                    var container = $("<div style='margin: 5px;'></div>");
                    toolbar.append(container);
                    container.append('<input class="btn btn-inverse" id="addrowbutton" type="button" value="Add New Row" />');
                    //container.append('<input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Delete Selected Row" />');
                    //container.append('<input style="margin-left: 5px;" id="updaterowbutton" type="button" value="Update Selected Row" />');
                    //container.append('<input style="margin-left: 5px;" id="saverows" type="button" value="Save Changes" />');
                    $("#addrowbutton").jqxButton();
                    //$("#deleterowbutton").jqxButton();
                    //$("#updaterowbutton").jqxButton();
                    //$("#saverows").jqxButton();
                    // update row.
                    //$("#updaterowbutton").on('click', function () {
                    //    var datarow = generaterow();
                    //    var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                    //    var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                    //    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    //        var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    //        var commit = $("#jqxgrid").jqxGrid('updaterow', id, datarow);
                    //        $("#jqxgrid").jqxGrid('ensurerowvisible', selectedrowindex);
                    //    }
                    //});

                    // create new row.
                    $("#addrowbutton").on('click', function () {
                        rowNum++;
                        var commit = $("#jqxgrid").jqxGrid('addrow', null, {id:"new"+rowNum, product_id:"", description:"", active: true, price: 0});
                        
                        datainfo = $("#jqxgrid").jqxGrid("getdatainformation");
                        rowcount = datainfo.rowscount;
                        $("#jqxgrid").jqxGrid("begincelledit", rowcount-1, "product_id");
                    });

                    // delete row.
                    //$("#deleterowbutton").on('click', function () {
                    //    var selectedrowindex = $("#jqxgrid").jqxGrid('getselectedrowindex');
                    //    var rowscount = $("#jqxgrid").jqxGrid('getdatainformation').rowscount;
                    //    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                    //        var id = $("#jqxgrid").jqxGrid('getrowid', selectedrowindex);
                    //        var commit = $("#jqxgrid").jqxGrid('deleterow', id);
                    //    }
                    //});
                }
            });
        });
    </script>
  </head>

  <body>

    <?php require("includes/navigation.php"); ?>

    <div class="container">
        
      <div class="starter-template">
        <div id="jqxgrid"></div>
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="../bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
