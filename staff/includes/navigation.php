    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Tori Grill</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li id="home_menu"><a href="#">Home</a></li>
            <li class="dropdown" id="products_menu">
		<a href="#" data-toggle="dropdown" class="dropdown-toggle">Products <b class="caret"></b></a>
		<ul class="dropdown-menu">
		    <li><a href="product_mtn.php">Product Maintenance</a></li>
		    <li><a href="menu_mtn.php">Menu Maintenance</a></li>
		    <!--li class="divider"></li-->
		</ul>
	    </li>
            <li class="dropdown" id="inventory_menu">
		<a href="#" data-toggle="dropdown" class="dropdown-toggle">Inventory<b class="caret"></b></a>
		<ul class="dropdown-menu">
		    <li><a href="inventory_mgt.php">Inventory Management</a></li>
		    <li><a href="inventory_mvt.php">Inventory Movement</a></li>
		    <!--li class="divider"></li-->
		</ul>
	    </li>
            <li class="dropdown" id="sales_menu">
		<a href="#" data-toggle="dropdown" class="dropdown-toggle">Sales <b class="caret"></b></a>
		<ul class="dropdown-menu">
		    <li><a href="sales_entry.php">Sales Entry</a></li>
		    <li><a href="sales_report.php">Sales Report</a></li>
		    <!--li class="divider"></li-->
		</ul>
	    </li>
          </ul>
	  <ul class="nav navbar-nav pull-right">
			<li class="active"><a href="#">You are logged in as <span id="username"><?=$username?></span></a></li>
            <li><a href="includes/logout.php">Logout</a></li>
	  </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>